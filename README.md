# someone-yells-k8s demo webapp

demo k8s web-application on node.js + express + pug - based on demo webapp [gitlab.com/henrybravo/someone-yells](https://gitlab.com/henrybravo/someone-yells)

***New branch of the demo to run this in serverless k8s with EKS on Fargate*** [here](https://gitlab.com/henrybravo/someone-yells-k8s/tree/fargate)

# type: Deployment

A ReplicaSet ensures that a specified number of pod replicas are running at any given time, where a Deployment is a higher-level concept that manages ReplicaSets and provides declarative updates to pods along with a lot of other useful features. Therefore, we recommend using Deployments instead of directly using ReplicaSets, unless you require custom update orchestration or don’t require updates at all. This actually means that you may never need to manipulate ReplicaSet objects: use a Deployment instead and define your application in the spec section. 

# deploy the demo

In this example, we deploy the webapp into [Amazon EKS](https://aws.amazon.com/eks/)

## step 1: create the webapp pods (we start with 3 pods)

```
$ kubectl create -f https://gitlab.com/henrybravo/someone-yells-k8s/raw/master/someone-yells-deployment.yaml
```

### verify the deployment

to verify that the deployment was succesfull, run the kubectl deployments commands:

```
$ kubectl get deployment/someoneyellsv2 -o wide
NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE       CONTAINERS       IMAGES                 SELECTOR
someoneyellsv2   3         3         3            3           18m       someoneyellsv2   henrybravo/hostyells   name=someoneyellsv2
```

```
$ kubectl describe deployment/someoneyellsv2
Name:                   someoneyellsv2
Namespace:              default
CreationTimestamp:      Tue, 07 Aug 2018 13:22:49 +0200
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision=1
Selector:               name=someoneyellsv2
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  name=someoneyellsv2
  Containers:
   someoneyellsv2:
    Image:        henrybravo/hostyells
    Port:         3000/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   someoneyellsv2-78c885cb47 (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  47s   deployment-controller  Scaled up replica set someoneyellsv2-78c885cb47 to 3
```

to see each pod running, list the pods you created in your EKS cluster with the kubectl get pods command:

```
$ kubectl get pods -o wide
NAME                 READY     STATUS    RESTARTS   AGE       IP           NODE
someoneyellsv2-78c885cb47-6s8sp   1/1       Running   0          20m       10.0.0.46    ip-10-0-0-15.ec2.internal
someoneyellsv2-78c885cb47-llwb6   1/1       Running   0          20m       10.0.1.141   ip-10-0-1-86.ec2.internal
someoneyellsv2-78c885cb47-ztk2q   1/1       Running   0          20m       10.0.1.165   ip-10-0-1-86.ec2.internal
```

## step 2: create the webapp service

create the service in order to expose the webapp to the web by creating an AWS LoadBalancer

```
$ kubectl create -f https://gitlab.com/henrybravo/someone-yells-k8s/raw/master/someone-yells-service.yaml
service "someoneyellsv2" created
```

### verify that the service was created and an AWS LoadBalancer was created
 
to verify that the service is up, list the services you created in the cluster with the kubectl get services command:

```
$ kubectl get services -o wide
NAME           TYPE         CLUSTER-IP     EXTERNAL-IP                     PORT(S)      AGE SELECTOR
someoneyellsv2 LoadBalancer 172.20.186.150 ***.us-east-1.elb.amazonaws.com 80:31516/TCP 22m name=someoneyellsv2
```

The service is created with label name=someoneyellsv2 and the AWS LoadBalancer was created serving the application on port 80

## That's all folks!
